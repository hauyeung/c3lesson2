﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace csharp3_lesson2
{
    
    public partial class ResultsForm : Form
    {
        public ResultsForm()
        {
            
            InitializeComponent();
        }

        public void addtolist(string s)
        {
            resultlistBox.Items.Add(s);
        }

        public void clearlist()
        {
            resultlistBox.Items.Clear();
        }

        private void okbutton_Click(object sender, EventArgs e)
        {
            Close();
        }
     }
}

