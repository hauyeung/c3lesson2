﻿namespace csharp3_lesson2
{
    partial class ResultsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.resultlistBox = new System.Windows.Forms.ListBox();
            this.okbutton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // resultlistBox
            // 
            this.resultlistBox.FormattingEnabled = true;
            this.resultlistBox.Location = new System.Drawing.Point(13, 13);
            this.resultlistBox.Name = "resultlistBox";
            this.resultlistBox.Size = new System.Drawing.Size(259, 238);
            this.resultlistBox.TabIndex = 0;
            // 
            // okbutton
            // 
            this.okbutton.Location = new System.Drawing.Point(103, 273);
            this.okbutton.Name = "okbutton";
            this.okbutton.Size = new System.Drawing.Size(75, 23);
            this.okbutton.TabIndex = 1;
            this.okbutton.Text = "OK";
            this.okbutton.UseVisualStyleBackColor = true;
            this.okbutton.Click += new System.EventHandler(this.okbutton_Click);
            // 
            // ResultsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 321);
            this.Controls.Add(this.okbutton);
            this.Controls.Add(this.resultlistBox);
            this.Name = "ResultsForm";
            this.Text = "ResultsForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox resultlistBox;
        private System.Windows.Forms.Button okbutton;
    }
}